﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactAnim : MonoBehaviour
{
    private float destroyDelay = .4f;

    void Start()
    {
        Destroy(gameObject, destroyDelay);
    }
}