﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    public GameObject bulletPrefab;
    public GameObject bazookaBulletPrefab;
    public Transform bulletSpawnPos;

    private Rigidbody2D rb;

    public float moveSpeed;
    public Transform gunPivot;

    public float jumpForce;
    public Transform groundCheckPosition;
    public LayerMask layerToTest;
    private bool onTheGround;

    public Camera cam;

    //private bool isFacingRight = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        if (isLocalPlayer)
        {
            cam = FindObjectOfType<Camera>();
            cam.transform.parent = this.gameObject.transform;
            cam.transform.localPosition = new Vector3(0f, 0f, -10f);
        }
    }
    // Update is called once per frame
    void Update ()
    {
        if (!isLocalPlayer) return;

        Movement();
        Facing();
        Jump();
        if (Input.GetMouseButtonUp(0)) CmdFire();
        if (Input.GetMouseButtonUp(1)) CmdBazookaFire();

        if (Input.GetButtonDown("Cancel"))
        {
            if (!isClient)
            {
                CustomNetworkManager.singleton.StopClient();
                Network.Disconnect();
            }
            else
            {
                CustomNetworkManager.singleton.StopHost();
                Network.Disconnect();
            }
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

    private void Movement()
    {
        //float move = Input.GetAxis("Horizontal");.

        if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)  // moves when key pressed
        {
            rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, rb.velocity.y);
        }

        if (Input.GetAxisRaw("Horizontal") < 0.5f && Input.GetAxisRaw("Horizontal") > -0.5f) // stops movement when key not pressed
        {
            rb.velocity = new Vector2(0f, rb.velocity.y);
        }

        //if (move < 0 && isFacingRight) Flip();
        //if (move > 0 && !isFacingRight) Flip();
    }
        
    //void Flip()
    //{
    //    isFacingRight = !isFacingRight;
    //    transform.Rotate(Vector3.up * 180);
    //}

    void Facing() // aims gun at mouse position
    {
        Vector2 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        gunPivot.transform.up = direction;
    }

    [Command]
    void CmdFire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawnPos.position, bulletSpawnPos.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.up * 10;

        // Spawn the bullet on the Clients
        NetworkServer.Spawn(bullet);

        // Destroy the bullet after 3 seconds
        Destroy(bullet, 3.0f);
    }

    [Command]
    void CmdBazookaFire()
    {
        var bullet = (GameObject)Instantiate(bazookaBulletPrefab, bulletSpawnPos.position, bulletSpawnPos.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.up * 10;
        NetworkServer.Spawn(bullet);
        Destroy(bullet, 3f);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.yellow;
    }

    void Jump()
    {
        onTheGround = Physics2D.OverlapCircle(groundCheckPosition.position, 0.1f, layerToTest);

        if (onTheGround && Input.GetButtonDown("Jump"))
            rb.AddForce(new Vector2(0f, jumpForce));
    }
}
