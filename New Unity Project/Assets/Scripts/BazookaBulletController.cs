﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BazookaBulletController : MonoBehaviour
{
    public GameObject blastRadius;
    public GameObject impactAnim;

    //private CircleCollider2D cd;

    //private void Start()
    //{
    //    cd = GetComponent<CircleCollider2D>();
    //}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("aa"))
        {
            Instantiate(blastRadius, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            Instantiate(impactAnim, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            //cd.radius = 1f;
            //transform.position = collision.gameObject.transform.position;
            //Destroy(collision.gameObject);
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            Instantiate(blastRadius, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            Instantiate(impactAnim, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            Destroy(gameObject);
        }

        //if (collision.gameObject.CompareTag("Player"))
        //{
        //    var hit = collision.gameObject;
        //    var health = hit.GetComponent<PlayerHealth>();
        //    if (health != null)
        //    {
        //        health.TakeDamage(20);
        //    }
        //    Destroy(gameObject);
        //}

    }
}
