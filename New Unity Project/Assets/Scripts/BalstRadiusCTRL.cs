﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalstRadiusCTRL : MonoBehaviour
{
    private void Update()
    {
        gameObject.transform.position += new Vector3(0f, 0.01f, 0f);
        //Destroy(gameObject, 0.1f);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("aa"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            var hit = collision.gameObject;
            var health = hit.GetComponent<PlayerHealth>();
            if (health != null)
            {
                health.TakeDamage(20);
            }
            Destroy(gameObject);
        }

    }
}
